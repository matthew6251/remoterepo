public class HotelRoom { 
	private boolean isSmoking=false;
	public static void main(String [] argsIn) { 
		HotelRoom hr = new HotelRoom(false);
		HotelRoom hr2 = new HotelRoom(true);
		System.out.println(hr);
		System.out.println(hr2);
	}
	public HotelRoom() { }
	public HotelRoom(boolean isSmokingIn) { 
		isSmoking = isSmokingIn;
	}
	public void setIsSmoking(boolean isSmokingIn) { isSmoking = isSmokingIn; }
	public boolean isSmoking() { return isSmoking; }
	public String toString() { 
		return "HotelRoom:[isSmoking:" + isSmoking +"]";
	}
}